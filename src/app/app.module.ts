import { AuthenticationService } from './services/authentication.service';
import { FormService } from './services/form.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import { MdRadioGroup, MdRadioButton, MdRadioModule} from '@angular/material';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { CandidateComponent } from './components/login/components/candidate/candidate.component';
import { CompanyComponent } from './components/login/components/company/company.component';
import { httpFactory } from './services/http-factory.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CandidateComponent,
    CompanyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MdRadioModule,
    ReactiveFormsModule
  ],
  providers: [
    FormService,
    AuthenticationService,
    {
    provide: Http,
    useFactory: httpFactory,
    deps: [XHRBackend, RequestOptions]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

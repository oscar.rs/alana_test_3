import { Injectable } from '@angular/core';
import {  Http } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {

  constructor(private http: Http) { }

  access(user): any {
   return this.http.post('/secure-candidate/login_check', user)
    .map(data => {
      return data.json();
    });
  }

}

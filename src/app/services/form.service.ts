import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
@Injectable()
export class FormService {
  private name: string;
  private type: string;
  
  constructor() { }
  
   createForm(regex?) :FormGroup{
     let userValidator = regex ? [Validators.required, Validators.pattern(regex)] : Validators.required;
     return new FormGroup({
      user: new FormControl('', userValidator),
      password: new FormControl('', [Validators.required])
    })
   }
  
}

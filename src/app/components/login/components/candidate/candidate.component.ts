import { AuthenticationService } from './../../../../services/authentication.service';
import { Candidate } from './../../../../interfaces/candidate';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormService } from './../../../../services/form.service';


@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css']
})
export class CandidateComponent{

  @Input() show: boolean;
  @Output() greet: EventEmitter<Object> = new EventEmitter;
  private form: FormGroup;
  private submit: boolean;
  private title = 'Alana Test 2';
  private subtitle = 'Acceso de candidatos';
  private candidate: Candidate;
  
  constructor(private _formService: FormService, private _authenticationService: AuthenticationService) {
    this.form =  this._formService.createForm();
  }

  access(): void {
    this.submit = true;
    if (this.form.valid) {
      // this.greet.emit({user: this.form.get('user').value, 
      //                                       type: 'candidate'})
      this.candidate = this.form.value;
      let _body: string = `email=${this.candidate.user}&password=${this.candidate.password}`;
      // console.log(_body);
      this._authenticationService.access(_body).subscribe( res => {
        console.log(res);
      });
    }
  }
}

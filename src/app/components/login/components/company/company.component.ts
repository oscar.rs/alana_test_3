import { Company } from './../../../../interfaces/company';
import { FormService } from './../../../../services/form.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent{

  @Input() show: boolean;
  @Output() greet:EventEmitter<Object> = new EventEmitter;
  private title = 'Alana Test 2';
  private subtitle = 'Acceso de Empresas';
  private form: FormGroup;
  private submit: boolean;
  private company: Company;

  
  constructor(private _formService: FormService) {    
    this.form = this._formService.createForm(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{1,63}$/)
  }

  private access(): void{
    this.submit = true;
    if (this.form.valid) {      
      this.greet.emit({user: this.form.get('user').value, 
                                            type: 'company'})
      this.company = this.form.value;
    }

  }


}

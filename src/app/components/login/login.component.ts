import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{

  private forms: Object = {
    candidate: false,
    company: false
  }

  private setType(type): void{
    if (!this.forms[type]) this.forms[type] = !this.forms[type];
    type === 'candidate' ? this.forms['company'] = false : this.forms['candidate'] = false 
  }

  private greet(user: Object): void{
    alert(`Bienvenido ${user['user']} ha ingresado como ${this.translate(user['type'])} `)
  }

  private translate(type: string): string{
    return type === 'company' ? 'Empresa' : 'Candidato' 
  }

}
